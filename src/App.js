import logo from './logo.svg';
import './App.css';
import HomePage from "./components/HomePage"
import NavBar from "./components/NavBar"
import Cart from "./components/Cart"
import { Route, Switch } from 'react-router-dom';
import PonePage from './components/PonePage';

const App = () => (
  <div className="App">
    <NavBar />
    <Switch >
      <Route path="/" exact component={HomePage} />
      <Route path="/cart" component={Cart} />
      <Route path='/phone/:id' component={PonePage} />
    </Switch>
  </div>
);

export default App;
