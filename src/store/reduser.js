import {aTypes} from "./../components/ActionTypes"

const initialState = {
    rowNumber: "col-3",
    sortOrder: null,
    phoneOrders: []
}

const reduser = (state = initialState, action) => {
    switch (action.type) {
        case aTypes.row3:
            return { ...state, rowNumber: 4 }
        case aTypes.row4:
            return { ...state, rowNumber: 3 }
        case aTypes.highToLow:
            return { ...state, sortOrder: "HTL" }
        case aTypes.lowToHigh:
            return { ...state, sortOrder: "LTH" }
        case aTypes.changeOrder:
            return { ...state, phoneOrders: action.newOrder }
        default:
            return state;
    }
}

    export default reduser